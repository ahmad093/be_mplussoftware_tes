'use strict';
const router = require('express').Router();
module.exports = function(app){
    var todoListcontrollerBook = require('./controllers/bookcontroller');
    var todoListcontrollerTypeBook = require('./controllers/typebookcontroller');

    //books
    app.route('/api/v1/getbooksmaster')
        .get(todoListcontrollerBook.getBookMaster);

    app.route('/api/v1/addbooksmaster')
        .post(todoListcontrollerBook.addBookMaster);

    app.route('/api/v1/delbooksmaster/:id')
        .delete(todoListcontrollerBook.delBookmaster);

    app.route('/api/v1/updbooksmaster/:id')
        .put(todoListcontrollerBook.updateBookmaster);

    app.route('/api/v1/searchBook')
        .post(todoListcontrollerBook.searchBook);

    // type book
    app.route('/api/v1/getTypebook')
        .get(todoListcontrollerTypeBook.getTypeBook);

    app.route('/api/v1/addTypebook')
        .post(todoListcontrollerTypeBook.addtypeBook);

    app.route('/api/v1/delTypebook/:id')
        .delete(todoListcontrollerTypeBook.delTypeBook);

    app.route('/api/v1/updTypebook/:id')
        .put(todoListcontrollerTypeBook.updateTypeBook);

}
