'use strict';

const typeBookModel         = require('../models/typeBook');
var timesDate = new Date();

exports.getTypeBook = function(req,res){
    console.log('masuk get')
    typeBookModel.tblTypeBook().findAll({
        attributes:['idtype','typebook']
      }).then((response) => {
          res.status(200).json({
            message :'OK',
            times   : timesDate,
            data    : response
          })
      }).catch((err)=>{
          res.status(500).json({
              message:err.message,
              times   : timesDate
          });
      }); 

}

exports.addtypeBook = function(req,res){
    console.log('masuk add');

    var typeBookObj = req.body.typeBookObj;
    console.log(typeBookObj)

    typeBookModel.tblTypeBook().create({
        typebook : typeBookObj
    }).then((typebookres) => {
        res.status(200).json({
            status : 200,
            message : 'Success add Type Book',
            times   : timesDate,
            data    : typebookres
        });
    }).catch((err) => {
        res.status(500).json({
            status : 500,
            message : err.message,
            times   : timesDate
        });
    });
}

exports.delTypeBook = function(req,res){
    console.log('masuk del');

    const id = req.params.id;
    console.log(id);

    var condition = {
        idtype : id
    }
    typeBookModel.tblTypeBook().destroy(
        {where:condition}
    ).then((typebookres) => {
        res.status(200).json({
            status : 200,
            message : `Success deleted a type book`,
            times   : timesDate,
            data    :  typebookres
        });
    }).catch((err) => {
        res.status(404).json({
            status : 404,
            message : err.message,
            times   : timesDate
        });
    });
}

exports.updateTypeBook = function(req,res){
    console.log('masuk update');
    const id        = req.params.id
    var typeBookObj = req.body.typeBookObj;
    console.log(typeBookObj);

    var condition = {idtype : id}

    typeBookModel.tblTypeBook().update({
        typebook : typeBookObj
    },
    {where : condition})
    .then((typebookres) => {
        res.status(200).json({
            status : 200,
            message     : `Success updated a tyep book`,
            timesDate   : timesDate,
            data        : typebookres
        });
    }).catch((err) => {
        res.status(404).json({
            status : 404,
            message : err.message,
            times   : timesDate
        });
    });
}