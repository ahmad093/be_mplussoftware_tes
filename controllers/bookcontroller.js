'use strict';

const bookModel         = require('../models/bookmaster');
const Op = require('sequelize').Op;
var timesDate = new Date();


exports.getBookMaster = function(req,res){
    // console.log('masuk get')
  bookModel.tblBookMaster().findAll({
    attributes:['idbook','title','author','date','numpage','typebook']
  }).then((response) => {
      res.status(200).json({
        message:'OK',
        data : response
      })
  }).catch((err)=>{
      res.status(500).json({
          message:err.message
      });
  }); 
}

exports.addBookMaster = function(req, res){
// console.log('masuk post')
    var titleObj    = req.body.titleObj;
    var authorObj   = req.body.authorObj;
    var dateObj     = req.body.dateObj;
    var numpageObj  = req.body.numpageObj;
    var typebookObj = req.body.typebookObj;

    console.log(titleObj,
        authorObj,
        dateObj,
        numpageObj,
        typebookObj)

    bookModel.tblBookMaster().create({
        title       : titleObj,
        author      : authorObj,
        date        : dateObj,
        numpage     : numpageObj,
        typebook    : typebookObj
    }).then((bookres)=>{
        res.status(200).json({
            status : 200,
            message : 'Success add Book',
            times   : timesDate,
            data    : bookres
        });
    }).catch((err)=>{
        res.status(500).json({
            status : 500,
            message : err.message,
            times   : timesDate
        });
    });
}

exports.delBookmaster = function(req, res){
    console.log('masuk del')
    const id = req.params.id;
    // console.log(id);
    var condition = {
        idbook : id
    }
    bookModel.tblBookMaster().destroy(
        {where:condition}
    ).then((bookres) => {
        res.status(200).json({
            status : 200,
            message : `Success deleted a book`,
            times   : timesDate,
            data    :  bookres
        });
    }).catch((err)=>{
        res.status(404).json({
            status : 404,
            message : err.message,
            times   : timesDate
        });
    });
}

exports.updateBookmaster = function(req,res){
    // console.log('masuk update');
    const id        = req.params.id;
    var titleObj    = req.body.titleObj;
    var authorObj   = req.body.authorObj;
    var numpageObj  = req.body.numpageObj;
    var typebookObj = req.body.typebookObj;

    // console.log(titleObj,
        // authorObj,
        // numpageObj,
        // typebookObj);

    var condition   = {idbook : id}
    
    bookModel.tblBookMaster().update({
        title       : titleObj,
        author      : authorObj,
        numpage     : numpageObj,
        typebook    : typebookObj
    },
    {where : condition})
    .then((bookres) => {
        res.status(200).json({
            status      : 200,
            message     : `Success updated a Book`,
            timesDate   : timesDate,
            data        : bookres
        });
    }).catch((err) => {
        res.status(404).json({
            status  : 404,
            message : err.message,
            times   : timesDate
        });
    });
}

exports.searchBook = function(req,res){
    let titleBook = req.body.titleBook
    console.log(titleBook)

    bookModel.tblBookMaster().findAll({
        where: {
            title: {
                [Op.like] : `%${titleBook}%`
            }
          }
    }).then((bookres) => {
        res.status(200).json({
            message:'OK',
            times : timesDate,
            data : bookres
          })
    }).catch((err) => {
        res.status(500).json({
            message:err.message,
            times : timesDate,
        });
    });
}


