'use strict';

var Sequelize   = require('sequelize');
var db_con      = require('../connector/connector');
var config      = require('../config/config');

var sequelize = db_con(config.db_msoftwaretest);

module.exports = {
    tblTypeBook : function(){
        const typeBook = sequelize.define('tb_typebook',{
            idtype : {
                type : Sequelize.INTEGER,
                primaryKey      : true,
                autoIncrement   : true
            },
            typebook : Sequelize.STRING
        },{
            freezeTableName : true,
            timestamps      : false
        });
        return typeBook;
    }
}