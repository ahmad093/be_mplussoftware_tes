'use strict';

var Sequelize   = require('sequelize');
var db_con      = require('../connector/connector');
var config      = require('../config/config');

var sequelize = db_con(config.db_msoftwaretest);

module.exports = {
    tblBookMaster : function(){
        const bookMaster = sequelize.define('tb_bookmaster',{
            idbook : {
                type            : Sequelize.INTEGER,
                primaryKey      : true,
                autoIncrement   : true
            },
            title       : Sequelize.STRING,
            author      : Sequelize.STRING,
            date        : Sequelize.DATE,
            numpage     : Sequelize.INTEGER,
            typebook    : Sequelize.STRING
        },{
            freezeTableName : true,
            timestamps      : false
        });
        return bookMaster;
    }

}