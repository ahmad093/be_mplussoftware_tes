var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    controller = require('./controllers/bookcontroller');

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

var routes = require('./routes');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    // res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
    // res.header("Access-Control-Allow-Headers", "X-Requested-With,Accept,Origin,Content-Type,token");
    // res.header('Access-Control-Allow-Credentials', true);
    return next();
  });


routes(app);

// app.listen(port);
// console.log('Learn Node JS With Kiddy, RESTful API server started on: ' + port);

app.listen(port, function() {
  console.log(`RESTful API Test M+ Software server started on ${port}`);
});