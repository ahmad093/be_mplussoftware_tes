-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2019 at 01:58 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_m+softwaretest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_bookmaster`
--

CREATE TABLE `tb_bookmaster` (
  `title` varchar(20) NOT NULL,
  `author` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `numpage` int(6) NOT NULL,
  `typebook` varchar(20) NOT NULL,
  `idbook` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_bookmaster`
--

INSERT INTO `tb_bookmaster` (`title`, `author`, `date`, `numpage`, `typebook`, `idbook`) VALUES
('How to Make Study', 'Imanuel', '2019-10-22', 10, 'ACADEMIC', 9),
('Stroy A Human #2020', 'Alexander', '2019-10-22', 5, 'NOVEL', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tb_typebook`
--

CREATE TABLE `tb_typebook` (
  `idtype` int(6) NOT NULL,
  `typebook` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_typebook`
--

INSERT INTO `tb_typebook` (`idtype`, `typebook`) VALUES
(5, 'FIKSI'),
(6, 'NOVEL'),
(8, 'RELIGION');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_bookmaster`
--
ALTER TABLE `tb_bookmaster`
  ADD PRIMARY KEY (`idbook`);

--
-- Indexes for table `tb_typebook`
--
ALTER TABLE `tb_typebook`
  ADD PRIMARY KEY (`idtype`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_bookmaster`
--
ALTER TABLE `tb_bookmaster`
  MODIFY `idbook` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_typebook`
--
ALTER TABLE `tb_typebook`
  MODIFY `idtype` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
