'use strict'

var fs              = require('fs');
var path            = require('path');
var Sequelize       = require('sequelize');

module.exports      = (config_param)=>{

    var sequelize   = new Sequelize(config_param.database, config_param.user, config_param.password, {
        host        : config_param.host,
        dialect     : 'mysql'
    });

    return sequelize
}